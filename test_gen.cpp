#include <iostream>
#include <stdio.h> // for sscanf
#include <stdlib.h>
#include <time.h>
#include <fstream>

using namespace std;


int main (int argc, char const *argv[])
{
    /* code */
    if (argc != 2)
    {
        cout << "usage: ./test_gen <data_size>" << endl;
        return 1;
    }
    int num = atoi (argv[1]);

    char pre;

    int cnt = 0;
    for (int i = 0; argv[1][i]; ++i)
    {
        if (argv[1][i] == '0')
        {
            cnt++;
        }
        else if (argv[1][i] != '0')
        {
            pre = argv[1][i];            
        }
    }

    ofstream fs;
    char filename[20];
    sprintf (filename, "test_case_%c_%d.txt", pre, cnt);
    fs.open (filename);

    srand ( time (NULL) );

    int x;
    for (int i = 1 ; i <= num ; i++)
    {
        x = rand() % 10000 + 1 ;
        fs << i << " " << x << endl;
        // cout << x << endl ;
    }

    fs.close();
    return 0;

}
