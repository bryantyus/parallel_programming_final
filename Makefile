CC = mpic++
CFLAG = -g -std=c++11

OBJ = domination_mpi.cpp

all: $(OBJ)
	$(CC) $(CFLAG) $(OBJ) -o dominate_mpi -lm

.PHONY: clean


clean:
	@rm *.o
