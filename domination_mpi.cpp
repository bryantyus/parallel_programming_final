/*
    domination_mpi.cpp is used to calculate the point dominate
    using MPI to implement it.
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "mpi.h"
#include <unistd.h>
#include <time.h>
#include <chrono>

using namespace std;
using namespace std::chrono;
int * result ;


void master_dominate(int num, int *master_local_result)
{
    int index = 3, max = 0;

    result = (int*) malloc(sizeof(int) * num);

    memset(result, 0, sizeof(result));
    result[0] = master_local_result[0];
    result[1] = master_local_result[1];

    // for (int i = 0; i < num; i += 2)
    //     printf("master_res[%d]: %d, master_res[%d]: %d\n", i, master_local_result[i], i + 1, master_local_result[i+1]);
    // max = result[1];

    // printf("max: %d\n", max);

    // clock_t begin_time = clock();
    // printf("Begin in master processor\n");

    for (int i = 3; i <= num; i += 2)    
    {
        if (master_local_result[i] >= max)
        {
            max = master_local_result[i];
            // printf("max: %d\n", max);
            result[index - 1] = master_local_result[i - 1];
            // X cordinate
            result[index] = master_local_result[i];
            // Y cordinate
            index += 2;
        }
    }

    // cout << "Time for Master processor : " << (float) (clock() - begin_time) / CLOCKS_PER_SEC << endl;
    // cout << "-----------------------------------------" << endl << endl;

    // cout << "----" << endl;
    // from small X coordinate to large X coordinate
    // for (int i = 0; i <= index - 2; i += 2 )
    // {
    //     cout << "X : " << result[i] << ", Y : " << result[i + 1] << endl;
    // }

    // Correct output with sequential.
    // for (int i = index - 2; i >= 0; i -= 2)
    // {
    //     cout << "X : " << result[i - 1] << ", Y : " << result[i] << endl;
    // }


}

void do_point_domination (int * points , int num)
{
    // clock_t begin_time = clock();
    auto start = high_resolution_clock::now();
    int rank, processor_number;

    int index = 1, max = 0 ;
    // result[0] = points[2 * (num - 1)];
    // result[1] = points[2 * (num - 1) + 1];
    // max = result[1];
    max = points[2 * (num - 1) + 1];
    // result[1] : Max(y)

    int local_index = 1;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &processor_number);
    MPI_Request request;
    MPI_Status status;

    int local_part = num / processor_number;


    if (rank == 0)
    {
        int end = (processor_number - 1 - rank) * local_part;
        int *local_result = (int*) malloc(sizeof(int) * local_part * 2);
        memset(local_result, 0, sizeof(local_result));

        local_result[0] = points[2 * (num - 1)];
        local_result[1] = points[2 * (num - 1) + 1];

        // printf("Rank |%d| , end : |%d| , start : |%d|\n", rank, end, num - 2);

        // max = result[1];
        for (int i = num - 2; i >= end; i--)
        {
            if (points[2 * i + 1] >= max )
            {
                local_result[2 * local_index] = points[2 * i];
                local_result[2 * local_index + 1] = points[2 * i + 1];
                max = points[2 * i + 1];
                local_index++;
            }
        }

        int *rec = (int*) malloc(sizeof(int) * local_part * 2);

        // size_t rec_size = (sizeof(int) * local_part * 2);
        int rec_size;
        int start_append_pos = local_index * 2;

        // for (int i = 0; i < local_index * 2; ++i)
        // {
        //     printf("processor |%d| seq.number |%d| local_result : |%d|\n", rank, i, local_result[i]);
        // }
        // Receive.

        // clock_t begin_time = clock();
        // printf("Begin for Receive data from other processors\n");


        for (int source = 1; source < processor_number; source++)
        {
			memset(rec, 0, sizeof(rec));

            MPI_Recv(&rec_size, 1/* receive what size Processor 0 should receive */, MPI_INT, source, source, MPI_COMM_WORLD, &status);

            // printf("Processor|%d| should receive size: %d from source |%d|\n", rank, rec_size, source);

            MPI_Recv(rec, rec_size, MPI_INT, source, source /* rank */, MPI_COMM_WORLD, &status);

            if (status.MPI_ERROR != 0)
            {
                cout << "Err: " << status.MPI_ERROR << endl;
            }
            for (int i = 0; i < rec_size ;i++)
            {
                local_result[start_append_pos + i] = rec[i];
            }

            start_append_pos += rec_size;
            // printf("start_append_pos: %d\n", start_append_pos);
            // cout << "Receive from process |" << status.MPI_SOURCE << "| ; with tag |" << status.MPI_TAG << "| ; error: " << status.MPI_ERROR << endl;
        }
        // cout << "Time for Receive : " << (float) (clock() - begin_time) / CLOCKS_PER_SEC << endl;
        // cout << "-----------------------------------------" << endl << endl;
        // for (int i = 0; i < start_append_pos; i = i + 2)
        // {
        //     printf("local_res[%d]: %d, local_res[%d]: %d\n", i, local_result[i], i+1, local_result[i+1]);
        // }
        
        // After Processor 0 receives all other message from other processors
        // Start to handle the dominate query.
        master_dominate(start_append_pos, local_result);

		free(rec);
        free(local_result);

        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<microseconds>(stop - start);
        cout << "Time for Master processor do all tasks : " << (float)(duration.count()) / 1000000 << endl;
        cout << "-----------------------------------------" << endl << endl;

    }
    else
    {
		// Other rank.
		int start = (processor_number - rank) * local_part - 1;
		int end = (processor_number - 1 - rank) * local_part;
        size_t send_size;
        int *local_result = (int*) malloc(sizeof(int) * local_part * 2);
        memset(local_result, 0, sizeof(local_result));

        // printf("Rank |%d| , end : |%d| , start : |%d|\n", rank, end, start);

		int local_max = max;
        for (int i = start; i >= end; --i)
        {
            if (points[2 * i + 1] >= local_max )
            {
                local_result[2 * local_index] = points[2 * i];
                local_result[2 * local_index + 1] = points[2 * i + 1];
				// printf("point[%d]: %d\n", 2*i + 1, points[2 * i + 1]);
				// printf("point[%d]: %d\n", 2*i, points[2 * i]);
                local_max = points[2 * i + 1];

                local_index++;
            }
        }
        send_size = local_index * 2;

        // for (int i = 0; i < send_size; ++i)
        // {
        //     printf("processor |%d| local_result[%d]: %d\n", rank, i, local_result[i]);
        // }


        // printf("Begin Send data from processor |%d|\n", rank);
        // clock_t begin_time = clock();        

        MPI_Send(&send_size, 1, MPI_INT, 0, rank, MPI_COMM_WORLD);
        // Notify that the processor 0 should receive size.
        // printf("Processor |%d| should send size: %d\n", rank, send_size);

        // Send
        MPI_Send(local_result, send_size, MPI_INT, 0/* processor 0 */ , rank, MPI_COMM_WORLD);

        // cout << "Time for Send data to Master from processor |" << rank << "| : " << (float) (clock() - begin_time) / CLOCKS_PER_SEC << endl;
        // cout << "-----------------------------------------" << endl << endl;


        free(local_result);
    }
}

int main (int argc, char *argv[])
{
    // read file
    if (argc != 2)
    {
        cout << "usage: ./domination <data_size>" << endl;
        return 1;
    }
    int rank, processor_number;

    int num = atoi (argv[1]);;

    char pre;

    int cnt = 0;
    for (int i = 0; argv[1][i]; ++i)
    {
        if (argv[1][i] == '0')
        {
            cnt++;
        }
        else
        {
            pre = argv[1][i];
        }
    }


    ifstream fs;
    char filename[50];
    sprintf (filename, "test_case_%c_%d.txt", pre, cnt);
    fs.open (filename);


    int * points = (int *)malloc (sizeof (int) * 2 * num);
    // result  = (int*) malloc (sizeof (int) * 2 * num);
    for ( int i = 0 ; i < num ; i ++ )
    {
        fs >> points[2 * i];
        fs >> points[2 * i + 1];
    }
    fs.close();


    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Barrier(MPI_COMM_WORLD);

    do_point_domination (points, num) ;

    // MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // MPI_Comm_size(MPI_COMM_WORLD, &processor_number);




    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    // chdir("result/");

    // ofstream fout;
    // fout.open(filename);
    // cout << "----" << endl;

    // for (int i = result_size - 1 ; i >= 0 ; i-- )
    // {
    //     fout << "X: " << result[2 * i] << ", " << "Y: " << result[2 * i + 1] << endl;
    // }
    // fout.close();

    // free (result);
    free (points);
    return 0;

}
