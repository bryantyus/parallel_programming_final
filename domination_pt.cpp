#include <iostream>
#include <stdio.h> // for sscanf
#include <stdlib.h>
#include <fstream>
#include <pthread.h>
#include <string.h> // for memcpy

#include <chrono> 
using namespace std::chrono;

using namespace std;
int * result;
int * points;
int * wl_rs; // workload and start point and return_size
int * final_result;

int do_point_domination_final(int * points , int num)
{
    int index = 1, max = 0 ;
    result[0] = points[0];
    result[1] = points[1];
    max = result[1];
    for(int i = 1 ; i < num ; i++)
    {
        if (points[2*i+1] >= max )
        {
            result[2*index] = points[2*i];
            result[2*index+1] = points[2*i+1];
            max = points[2*i+1] ;
            index++;
        }
    }
    return index;
}

void * do_point_domination(/*int * points , int start_from , int num*/void* rank)
{
    long my_rank = (long)rank;
    // setup workload.

    // int result_size = do_point_domination(points, wl_rs[2*tid] ,  wl_rs[2*tid+1]) ;
    int start_from = wl_rs[2*my_rank] ;
    int num = wl_rs[2*my_rank+1];

    int index = 1, max = 0 ;
    
    //offset
    int * result_local = result + start_from *2 ;
    int * points_local = points + start_from *2 ;

    // cout << result << "," << result_local << endl;

    result_local[0] = points_local[2*(num-1)];
    result_local[1] = points_local[2*(num-1)+1];
    max = result_local[1];
    for(int i = num-2 ; i >=0 ; i--)
    {
        if (points_local[2*i+1] >= max )
        {
            result_local[2*index] = points_local[2*i];
            result_local[2*index+1] = points_local[2*i+1];
            max = points_local[2*i+1] ;
            index++;
        }
    }

    wl_rs[2*my_rank+1] = index;

    return NULL;
}

int main(int argc, char const *argv[])
{
    // read file
    if(argc!=3){
        cout<< "usage: ./domination <data_size> <thread_num>"<<endl;
        return 1;
    }
    int num = atoi(argv[1]);
    int thread_num = atoi (argv[2]);

    pthread_t* thread_handles;
    thread_handles = (pthread_t*) malloc (thread_num*sizeof(pthread_t));
    
    wl_rs = (int *)malloc(sizeof(int) * thread_num * 2);

    int workload = num / thread_num ;
    for (int i = 0 ; i  <  thread_num ; i++ )
    {
        if ( i == thread_num - 1 )
        {
            wl_rs[2*i]  =  i * workload ;
            wl_rs[2*i+1] =  workload + (num % thread_num) ;
        }
        else
        {
            wl_rs[2*i] =  i * workload ;
            wl_rs[2*i+1] =  workload ;
        }
        
    }

    ifstream fs;
    char filename[20];
    sprintf(filename,"%d",num);
    fs.open(filename);
    points = (int *)malloc(sizeof(int) * 2 * num);
    result  =(int*) malloc(sizeof(int) * 2 * num);
    
    // load file to memory.
    for ( int i = 0 ; i < num ; i ++ )
    {
        fs >> points[2*i];
        fs >> points[2*i+1];
    }
    fs.close();

    auto start = high_resolution_clock::now();


    for(long thread = 0; thread < thread_num; thread++){
        pthread_create(&thread_handles[thread], NULL,do_point_domination,(void*)thread);
    }
    for (long thread = 0; thread < thread_num; thread++){
        pthread_join(thread_handles[thread], NULL);
    }

    int sum_size = 0;
    for ( int i = 0 ; i < thread_num ; i++ )
    {
        sum_size += wl_rs[2*i+1] ; 
    }

    int * fianl_points = (int *)malloc(sizeof(int) * 2 * sum_size); 
    final_result = (int *)malloc(sizeof(int) * 2 * sum_size);

    int offset = 0 ;
    int * p;
    for( int i = thread_num-1 ; i >= 0 ; i--)
    {
        p = fianl_points+offset;
        memcpy(p, &result[wl_rs[2*i]*2], (int)sizeof(int) * wl_rs[2*i+1] * 2 );
        offset += 2* wl_rs[2*i+1];
    }

    int result_size = do_point_domination_final(fianl_points, sum_size) ;

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    cout << "calculate: "<< (float)(duration.count())/1000000 << endl;
    // cout << "---" << endl;
    // for (int i = result_size - 1 ; i >=0 ; i-- )
    // {
    //     cout << result[2*i] << "," << result[2*i+1] << endl;
    // }
    cout << result_size << endl;

    // free pointer
    free(points);
    free(result);
    free(wl_rs);
    free(fianl_points);
    free(final_result);

    return 0;

}
