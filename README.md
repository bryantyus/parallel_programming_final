# This is the final project of the course of parallel programming

- We implement the dominate point algo. using three parallelizing models, which are PThread, OpenMP and MPI, separately.

## For Sequential version 
- Refer to domination.cpp

## For MPI version
- Refer to domination_mpi.cpp

## For OpenMP version
- Refer to domination_omp.cpp

## For Pthread version
- Refer to domination_pt.cpp
