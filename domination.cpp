#include <iostream>
#include <stdio.h> // for sscanf
#include <stdlib.h>
#include <fstream>
#include <time.h>
#include <chrono>

using namespace std;
using namespace std::chrono;
int * result ;

int dominat (int *a , int *b)
{
    if ( b[0] > a[0] && b[1] > a[1]) return 1;
    return 0;
}


int do_point_domination (int * points , int num)
{
    // clock_t begin_time = clock();
    auto start = high_resolution_clock::now();
    int index = 1, max = 0 ;
    result[0] = points[2 * (num - 1)];
    result[1] = points[2 * (num - 1) + 1];
    max = result[1];

    printf("Begin in master processor\n");
    
    for (int i = num - 2 ; i >= 0 ; i--)
    {
        if (points[2 * i + 1] >= max )
        {
            result[2 * index] = points[2 * i];
            result[2 * index + 1] = points[2 * i + 1];
            max = points[2 * i + 1] ;
            index++;
        }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    cout << "Time for Master processor : " << (float) (duration.count()) / 1000000 << endl;
    return index;
}

int main (int argc, char const *argv[])
{
    // read file
    if (argc != 2)
    {
        cout << "usage: ./domination <data_size>" << endl;
        return 1;
    }
    int num = atoi (argv[1]);;

    char pre;
    int cnt = 0;
    for (int i = 0; argv[1][i]; ++i)
    {
        if (argv[1][i] == '0')
        {
            cnt++;
        }
        else
        {
            pre = argv[1][i];
        }
    }


    ifstream fs;
    char filename[50];
    sprintf (filename, "test_case_%c_%d.txt", pre, cnt);
    fs.open (filename);
    int * points = (int *)malloc (sizeof (int) * 2 * num);
    result  = (int*) malloc (sizeof (int) * 2 * num);
    for ( int i = 0 ; i < num ; i ++ )
    {
        fs >> points[2 * i];
        fs >> points[2 * i + 1];
        // cout << points[2*i] << "," << points[2*i+1] << endl;
    }
    fs.close();

    int result_size = do_point_domination (points, num) ;
    // cout << "----" << endl;
    // for (int i = result_size - 1 ; i >= 0 ; i-- )
    // {
    //     cout << "X : " << result[2 * i] << ", Y : " << result[2 * i + 1] << endl;
    // }


    free (result);
    free (points);
    return 0;

}
